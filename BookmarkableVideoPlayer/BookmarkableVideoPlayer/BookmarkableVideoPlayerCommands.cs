﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace BookmarkableVideoPlayer
{
    public static class BookmarkableVideoPlayerCommands
    {
        public static readonly RoutedUICommand AddBookmark = new RoutedUICommand
            (
                "Add Bookmark",
                "Add Bookmark",
                typeof(BookmarkableVideoPlayerCommands),
                new InputGestureCollection()
                {
                    new KeyGesture(Key.B, ModifierKeys.Control)
                }
            );

        public static RoutedUICommand DeleteBookmark = new RoutedUICommand
            (
                "Delete Bookmark",
                "Delete Bookmark",
                typeof(BookmarkableVideoPlayerCommands),
                new InputGestureCollection()
                {
                    new KeyGesture(Key.D, ModifierKeys.Control)
                }
            );
    }
}
